<?php get_header(); ?>

<div class="blog-posts spacing-inside">
	<div class="container">
		<div class="posts-content">
			<?php if( is_home() && get_option( 'page_for_posts' ) ) : ?>

				<h1><?php echo apply_filters( 'the_title', get_post( get_option( 'page_for_posts' ) )->post_title ); ?></h1>

			<?php endif; ?>

			<ul class="post-list">
				<?php //<li class="grid-sizer"></li>       Uncomment if using Isotope ?>
				<?php while ( have_posts() ) : the_post();

					get_template_part( 'templates/template-parts/post-item' );

				endwhile; ?>
			</ul>

			<div class="pagination">
				<?php posts_nav_link( ' ', __( 'Prev', 'w10' ), __( 'Next', 'w10' ) ); ?>
				<button class="load-posts"><?php _e( 'Load more posts', 'w10' ); ?></button>
			</div>
		</div>

		<?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>
