/* global require */

var gulp			= require( 'gulp' );
var del				= require( 'del' );
var pump			= require( 'pump' );
var postcss			= require( 'gulp-postcss' );
var scss			= require( 'postcss-scss' );
var reporter		= require( 'postcss-reporter' );
var stylelint		= require( 'stylelint' );
var sourcemaps		= require( 'gulp-sourcemaps' );
var sass			= require( 'gulp-sass' );
var normalize		= require( 'postcss-normalize' );
var autoprefixer	= require( 'autoprefixer' );
var eslint			= require( 'gulp-eslint' );
var concat			= require( 'gulp-concat' );
var uglify			= require( 'gulp-uglify' );
var imagemin		= require( 'gulp-imagemin' );
var browsersync		= require( 'browser-sync' ).create();

var paths = {
	'scss': [
		'scss/*.scss',
		'scss/*/*.scss'
	],
	'js': [
		'js/*.js',
		'!js/load-posts.js'
	],
	'php': [
		'*.php',
		'*/*.php'
	],
	'img': 'img/*'
};

gulp.task( 'default', ['watch'], function() {



});

gulp.task( 'clean:css:dev', function() {

	return del( ['dev/css'] );

});

gulp.task( 'clean:css:dist', function() {

	return del( ['dist/css'] );

});

gulp.task( 'clean:js:dev', function() {

	return del( ['dev/js'] );

});

gulp.task( 'clean:js:dist', function() {

	return del( ['dist/js'] );

});

gulp.task( 'clean:img:dev', function() {

	return del( ['dev/img'] );

});

gulp.task( 'clean:img:dist', function() {

	return del( ['dist/img'] );

});

gulp.task( 'css:dev', ['clean:css:dev'], function() {

	return gulp.src( paths.scss )
		.pipe(
			sourcemaps.init()
		)
		.pipe(
			postcss( [
				stylelint(),
				reporter({ 'clearMessages': true /* throwError: true*/}),
			], { 'syntax': scss })
		)
		.pipe(
			sass({
				'errLogToConsole': true,
				'indentType': 'tab',
				'indentWidth': 1,
				'outputStyle': 'expanded',
			})
		)
		.pipe(
			postcss( [
				normalize({
					forceImport: true,
				}),
			] )
		)
		.pipe(
			postcss( [
				autoprefixer(),
			] )
		)
		.pipe(
			sourcemaps.write( '' )
		)
		.pipe(
			gulp.dest( 'dev/css' )
		)
		.pipe(
			browsersync.stream()
		);

});

gulp.task( 'css:dist', ['clean:css:dist'], function() {

	return gulp.src( paths.scss )
		.pipe(
			postcss( [
				stylelint(),
				reporter({ 'clearMessages': true /* throwError: true*/}),
			], { 'syntax': scss })
		)
		.pipe(
			sass({
				'errLogToConsole': true,
				'indentType': 'tab',
				'indentWidth': 1,
				'outputStyle': 'expanded',
			})
		)
		.pipe(
			postcss( [
				normalize({
					forceImport: true,
				}),
			] )
		)
		.pipe(
			postcss( [
				autoprefixer(),
			] )
		)
		.pipe(
			gulp.dest( 'dist/css' )
		)

});

gulp.task( 'img:dev', ['clean:img:dev'], function() {

	return gulp.src( paths.img )
		.pipe(
			imagemin()
		)
		.pipe(
			gulp.dest( 'dev/img' )
		);

});

gulp.task( 'img:dist', ['clean:img:dist'], function() {

	return gulp.src( paths.img )
		.pipe(
			imagemin()
		)
		.pipe(
			gulp.dest( 'dist/img' )
		);

});

gulp.task( 'img-watch', ['img:dev'], function( done ) {

	browsersync.reload();
	done();

});

gulp.task( 'js:dev', ['clean:js:dev'], function() {

	return gulp.src( paths.js )
		.pipe(
			sourcemaps.init()
		)
		.pipe(
			eslint()
		)
		.pipe(
			eslint.format()
		)
		.pipe(
			concat( 'main.js' )
		)
		.pipe(
			sourcemaps.write( '' )
		)
		.pipe(
			gulp.dest( 'dev/js' )
		);

});

gulp.task( 'js:dist', ['clean:js:dist'], function( cb ) {
	pump([
		gulp.src( paths.js ),
		eslint(),
		eslint.format(),
		eslint.failOnError(),
		concat( 'main.js' ),
		uglify(),
		gulp.dest( 'dist/js' ),
	  ],
	  cb
	);
});

gulp.task( 'js-watch', ['js:dev'], function( done ) {

	browsersync.reload();
	done();

});

gulp.task( 'watch', ['css:dev', 'img:dev', 'js:dev'], function() {

	gulp.watch( paths.scss, ['css:dev'] );
	gulp.watch( paths.img, ['img:dev'] );
	gulp.watch( paths.js, ['js:dev'] );

});

gulp.task( 'bs', ['css:dev', 'img:dev', 'js:dev'], function() {

	browsersync.init({
		'proxy': 'warp10.dev', // Change to whatever URL you need
		//host: '',
	});

	gulp.watch( paths.scss, ['css:dev'] );
	gulp.watch( paths.img, ['img-watch'] );
	gulp.watch( paths.js, ['js-watch'] );
	gulp.watch( paths.php ).on( 'change', browsersync.reload );

});

gulp.task( 'build', ['css:dist', 'img:dist', 'js:dist'], function() {



});
