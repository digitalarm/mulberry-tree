<form class="search-form" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="search" class="search-form__input" placeholder="<?php echo esc_attr_x( 'Search&hellip;', 'placeholder', 'twentysixteen' ); ?>" value="<?php echo get_search_query(); ?>" name="s">
	<button type="submit" class="search-form__submit button" aria-label="<?php _e( 'Search', 'w10' ); ?>"><span class="fa fa-search"></span></button>
</form>
