( function( $ ) {

	'use strict';

	document.documentElement.classList.remove( 'no-js' );

	$( function() {

		var $window				= $( window );
		var	$body				= $( 'body' );
		var	$sidebar			= $( '.sidebar' );
		var	$sidebarToggle		= $( '.sidebar__toggle' );
		var	$sidebarContent		= $( '.sidebar__content' );
		var smartresize;
		var smartscroll;


		/*
		* Mobile Menu
		*/
		$( '.menu-open' ).on( 'click', function() {

			$body.attr( 'data-menu-state', 'open' );

		});

		$( '.mobile-menu__close' ).on( 'click', function() {

			$body.attr( 'data-menu-state', 'closed' );

		});


		/*
		* Sidebar
		*/
		$sidebarToggle.on( 'click', function() {

			$sidebarContent.stop().slideToggle();
			if ( $sidebar.attr( 'data-mbl-state' ) === 'closed' ) {

				$sidebar.attr( 'data-mbl-state', 'open' );

			} else {

				$sidebar.attr( 'data-mbl-state', 'closed' );

			}

		});




		/*
		* Lazy load for images
		*
		* Target all images with .lazy class
		* <img src="" data-original="/path/image.png"> - Note, the SRC should be blank, using data-original to load in the image
		*/
		if ( $().lazyload ) {

			$( 'img.lazy' ).lazyload();

		}



		/*
		* Isotope
		*
		* Used to do grid layout and filtering on blog pages
		*/

		if ( $().isotope ) {

			$window.on( 'load', function() {

				$( '.post-list' ).isotope({
					'itemSelector': '.post-list__item',
					'masonry': { 'columnWidth': '.grid-sizer' },
				});

			});

		}


		/*
		* Smart resize and scroll
		*/

		function debounce( func, wait, immediate ) {

			var timeout;


			return function() {

				var context = this;
				var args = arguments;
				var later = function() {

					timeout = null;
					if ( ! immediate ) {

						func.apply( context, args );

					}

				};
				var callNow = immediate && ! timeout;

				clearTimeout( timeout );
				timeout = setTimeout( later, wait );
				if ( callNow ) {

					func.apply( context, args );

				}

			};

		}

		jQuery.fn.smartresize = function( fn ) {

			return fn ? this.bind( 'resize', debounce( fn, 100 ) ) : this.trigger( smartresize );

		};

		jQuery.fn.smartscroll = function( fn ) {

			return fn ? this.bind( 'scroll', debounce( fn, 100 ) ) : this.trigger( smartscroll );

		};

		$window.smartresize( function() {

			if ( window.matchMedia( 'screen and (min-width: 1025px)' ) ) {

				$body.attr( 'data-menu-state', 'closed' );
				$sidebarContent.removeAttr( 'style' );
				$sidebar.attr( 'data-mbl-state', 'closed' );

			}

		});

		$window.smartscroll( function() {

			// Event to happen on window scroll go here...

		});

	});

}( jQuery ) );
