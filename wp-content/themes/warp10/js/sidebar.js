( function( $ ) {

	'use strict';

	$( function() {

		var $window					= $( window );
		var $sidebarFixed			= $( '.sidebar--fixed' );
		var $sidebarFixedContent	= $sidebarFixed.children( '.sidebar-content' );
		var $container				= $sidebarFixed.parents( '.spacing-inside' );
		var $sidebarFixedSibling	= $sidebarFixed.siblings( '.page-content' );
		var windowHeight			= 0;
		var windowPos				= 0;
		var containerHeight			= 0;
		var containerPaddingTop		= 0;
		var containerPaddingBottom	= 0;
		var containerOffset			= 0;
		var sidebarContentHeight	= 0;
		var sidebarSiblingHeight	= 0;
		var sidebarOffset			= 0;
		var offsetDiff				= 0;


		/**
		* Fixed Sidebar
		*
		* Function to fix sidebar when scrolled past so that it moves with page. Will only do so page is taller than the sidebar
		*/
		function fixedSidebar() {

			if ( $sidebarFixed.length > 0 ) {

				windowHeight			= $window.innerHeight();
				windowPos				= $window.scrollTop();
				sidebarOffset			= $sidebarFixed.offset();
				sidebarContentHeight	= $sidebarFixedContent.innerHeight();
				sidebarSiblingHeight	= $sidebarFixedSibling.innerHeight();
				containerHeight			= $container.innerHeight();
				containerPaddingTop		= parseInt( $container.css( 'padding-top' ) );
				containerPaddingBottom	= parseInt( $container.css( 'padding-bottom' ) );
				containerOffset			= $container.offset();

				offsetDiff				= sidebarOffset.top - ( containerOffset.top + containerPaddingTop );

				// Check that the sidebar is shorter than the space it's in. If it isn't, sidebar will remain static
				if ( ( sidebarContentHeight < ( windowHeight - containerPaddingTop - containerPaddingBottom ) ) && ( sidebarContentHeight < sidebarSiblingHeight ) ) {

					// Check that the window has scrolled past the starting point of the sidebar. If it has, fix it.
					if ( windowPos > ( sidebarOffset.top - containerPaddingTop ) ) {

						$sidebarFixedContent.css({
							'position': 'fixed',
							'top': containerPaddingTop + 'px',
							'left': sidebarOffset.left + 'px',
						});

						// If the bottom of the sidebar reaches the bottom of its container it will be absolutely positioned there to stop it clipping into, for example, the footer.
						if ( ( windowPos + sidebarContentHeight ) >= ( containerHeight + containerOffset.top - containerPaddingTop - containerPaddingBottom ) ) {

							$sidebarFixedContent.css({
								'position': 'absolute',
								'top': ( containerHeight - sidebarContentHeight - offsetDiff - containerPaddingTop - containerPaddingBottom ) + 'px',
								'left': '0',
							});

						}

					} else {

						$sidebarFixedContent.removeAttr( 'style' );

					}

				} else {

					$sidebarFixedContent.removeAttr( 'style' );

				}

			}

		}

		$window.load( function() {

			if ( window.matchMedia( 'screen and (min-width: 1025px)' ) ) {

				fixedSidebar();

			}

			$window.scroll( function() {

				if ( window.matchMedia( 'screen and (min-width: 1025px)' ) ) {

					fixedSidebar();

				}

			});

			$window.smartresize( function() {

				if ( window.matchMedia( 'screen and (min-width: 1025px)' ) ) {

					fixedSidebar();

				} else {

					$sidebarFixedContent.removeAttr( 'style' );

				}

			});

		});

	});

}( jQuery ) );
