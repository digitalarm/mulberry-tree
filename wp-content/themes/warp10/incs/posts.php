<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
* Theme Support
*
*/
add_theme_support( 'post-thumbnails', array( 'post', 'service', 'product' ) );


/**
* Register custom post types
*
* https://codex.wordpress.org/Function_Reference/register_post_type
*/
function w10_custom_post_types() {

	register_post_type(
		'service',
		array(
			'labels' => array(
				'name'			=> __( 'Services', 'w10' ),
				'singular_name'	=> __( 'Service', 'w10' ),
				'all_items'		=> __( 'All Services', 'w10' ),
				'not_found'		=> __( 'Services', 'w10' ),
				'add_new_item'	=> __( 'Add New Service', 'w10' ),
				'edit_item'		=> __( 'Edit Service', 'w10' ),
				'view_item'		=> __( 'View Service', 'w10' ),
			),
			'public'		=> TRUE,
			'rewrite'		=> array( 'slug' => 'treatments' ),
			'menu_icon'		=> 'dashicons-store',
			'supports'		=> array( 'title', 'page-attributes', 'thumbnail' ),
			'has_archive'	=> FALSE,
		)
	);

	register_post_type(
		'product',
		array(
			'labels' => array(
				'name'			=> __( 'Products', 'w10' ),
				'singular_name'	=> __( 'Product', 'w10' ),
				'all_items'		=> __( 'All Products', 'w10' ),
				'not_found'		=> __( 'Products', 'w10' ),
				'add_new_item'	=> __( 'Add New Product', 'w10' ),
				'edit_item'		=> __( 'Edit Product', 'w10' ),
				'view_item'		=> __( 'View Product', 'w10' ),
			),
			'public'		=> TRUE,
			'rewrite'		=> array( 'slug' => 'product' ),
			'menu_icon'		=> 'dashicons-image-filter',
			'supports'		=> array( 'title', 'page-attributes', 'editor', 'thumbnail' ),
			'has_archive'	=> FALSE,
		)
	);

}
add_action( 'init', 'w10_custom_post_types' );


/**
* Register custom taxonomies
*
* https://codex.wordpress.org/register_taxonomy
*/
function w10_custom_taxonomies() {

	register_taxonomy(
		'types',
		'service',
		array(
			'labels' => array(
				'name'				=> __( 'Types', 'w10' ),
				'singular_name'		=> __( 'Type', 'w10' ),
				'search_items'		=> __( 'Search Type', 'w10' ),
				'all_items'			=> __( 'All Types', 'w10' ),
				'not_found'			=> __( 'No terms found.', 'w10' ),
				'parent_item'		=> __( 'Parent Type', 'w10' ),
				'parent_item_colon'	=> __( 'Parent Type:', 'w10' ),
				'update_item'		=> __( 'Update Type', 'w10' ),
				'edit_item'			=> __( 'Edit Type', 'w10' ),
				'add_new_item'		=> __( 'Add New Type', 'w10' ),
				'new_item_name'		=> __( 'New Type Name', 'w10' ),
				'menu_name'			=> __( 'Types', 'w10' ),
			),
			'hierarchical'		=> TRUE,
			'show_ui'			=> TRUE,
			'show_in_nav_menus'	=> TRUE,
			'public'			=> TRUE,
			'show_admin_column'	=> TRUE,
			'query_var'			=> TRUE,
			'rewrite'			=> array( 'slug' => 'spa' ),
		)
	);

	register_taxonomy(
		'product_category',
		'product',
		array(
			'labels' => array(
				'name'				=> __( 'Categories', 'w10' ),
				'singular_name'		=> __( 'Category', 'w10' ),
				'search_items'		=> __( 'Search Category', 'w10' ),
				'all_items'			=> __( 'All Categories', 'w10' ),
				'not_found'			=> __( 'No terms found.', 'w10' ),
				'parent_item'		=> __( 'Parent Category', 'w10' ),
				'parent_item_colon'	=> __( 'Parent Category:', 'w10' ),
				'update_item'		=> __( 'Update Category', 'w10' ),
				'edit_item'			=> __( 'Edit Category', 'w10' ),
				'add_new_item'		=> __( 'Add New Category', 'w10' ),
				'new_item_name'		=> __( 'New Category Name', 'w10' ),
				'menu_name'			=> __( 'Categories', 'w10' ),
			),
			'hierarchical'		=> TRUE,
			'show_ui'			=> TRUE,
			'show_in_nav_menus'	=> TRUE,
			'public'			=> TRUE,
			'show_admin_column'	=> TRUE,
			'query_var'			=> TRUE,
			'rewrite'			=> array( 'slug' => 'products' ),
		)
	);

}
add_action( 'init', 'w10_custom_taxonomies' );


/**
* Get page meta description
*
* @param post ID
*
* @return string meta description
*/
function get_metadesc( $post_id = 0 ) {

	$metadesc = get_post_custom_values( '_yoast_wpseo_metadesc', $post_id );
	return $metadesc[0];

}

function w10_order_category( $query ) {
	
	// exit out if it's the admin or it isn't the main query
	if ( is_admin() || ! $query->is_main_query() ) {
		return;
	}

	// order category archives by title in ascending order
	if ( is_taxonomy( 'types' ) ) {
		$query->set( 'order' , 'ASC' );
		//$query->set( 'orderby', 'title');
		return;
	}
}
add_action( 'pre_get_posts', 'w10_order_category', 1 );
