<?php get_header(); ?>

<div class="spacing-inside">
	<div class="container">

		<div class="navtrail"><a href="<?php echo site_url( 'treatments' ); ?>">Products</a> &raquo;</div>

		<h1><?php echo str_replace( 'Category:', '', get_the_archive_title() ); ?></h1>

		<div class="services">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php if ( has_post_thumbnail() ) :
					$img = get_the_post_thumbnail_url( get_the_ID(), 'medium' );
				endif; ?>

				<div class="service-item">
					<a href="<?php the_permalink(); ?>" class="service-item__inner">
						<span class="service-item__image" style="background-image: url('<?php echo $img; ?>');"></span>
						<span class="service-item__title"><?php the_title(); ?></span>
					</a>
				</div>

			<?php endwhile; ?>
		</div>

	</div>
</div>

<?php get_footer(); ?>

