<?php get_header(); ?>

<?php if ( get_option( 'page_for_posts' ) == 0 ) : ?>

	<?php $page_title = get_the_archive_title(); ?>

<?php else: ?>

	<?php $page_title = apply_filters( 'the_title', get_post( get_option( 'page_for_posts' ) )->post_title ) . ' - ' . get_the_archive_title(); ?>

<?php endif; ?>

<div class="blog-posts spacing-inside">
	<div class="container">
		<div class="posts-content">
			<h1><?php echo $page_title; ?></h1>

			<ul class="post-list">
				<?php //<li class="grid-sizer"></li>       Uncomment if using Isotope ?>
				<?php while ( have_posts() ) : the_post();

					get_template_part( 'templates/template-parts/post-item' );

				endwhile; ?>
			</ul>

			<div class="pagination">
				<?php posts_nav_link( ' ', __( 'Prev', 'w10' ), __( 'Next', 'w10' ) ); ?>
				<button class="button load-posts"><?php _e( 'Load more posts', 'w10' ); ?></button>
			</div>
		</div>

		<?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>
