		</main>
		<?php if ( ! is_page_template( 'page-templates/page-splash.php' ) ) : ?>
			<div class="block block-newsletter spacing-inside bg-grey">
				<div class="container">
					<div class="block-newsletter__form">
						<?php the_field('newsletter_form', 'option'); ?>
					</div>
				</div>
			</div>

			<footer class="footer">
				<div class="container">
					<div class="footer__treatments">
						<h4>Pages</h4>
						<?php if ( has_nav_menu( 'footer-menu' ) ) : ?>

							<div class="footer__nav">
								<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => 'nav', 'container_class' => '' ) ); ?>
							</div>

						<?php endif; ?>
					</div>	
					<div class="footer__opening-times">
						<h4>Opening Times</h4>
						<?php the_field ('opening_times', 'option' ); ?>
					</div>
					<div class="footer__address">
						<?php the_field ('address', 'option' ); ?> 
						<span class="footer__phone"><?php the_field ('telephone', 'option' ); ?></span>
					</div>			
				</div>
				<div class="footer__copy">
					<span class="footer__alt-menu"><?php wp_nav_menu( array( 'theme_location' => 'footer-alt-menu', 'container' => '', 'container_class' => 'alt-menu' ) ); ?></span>
					<?php echo str_replace( 'DATE', date( 'Y' ), get_theme_mod( 'w10_copyright_text', '&copy; Company Name DATE' ) ); ?>
					
				</div>
			</footer>


		<?php endif; ?>

		<?php if ( has_nav_menu( 'header-menu' ) ) : ?>

			<div class="mobile-menu">
				<span class="mobile-menu__close" aria-label="<?php _e( 'Close Menu', 'w10' ); ?>">&times;</span>
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'nav' ) ); ?>
			</div>

		<?php endif; ?>

		<div class="cookie-bar"><?php _e( 'This site uses cookies. By using this site you are agreeing to our use of cookies. Review our <a href="' . site_url('/privacy') . '">cookies policy</a> for more details.', 'w10'); ?><span class="cookie-bar__close button">Close</span></div>
		<?php if ( TRUE === get_theme_mod( 'gmaps_enabled', FALSE ) && '' !== get_theme_mod( 'gmaps_api_key', '' ) ) : ?>

			<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo get_theme_mod( 'gmaps_api_key', '' ); ?>"></script>

		<?php endif; ?>

		<?php wp_footer(); ?>
	</body>
</html>
