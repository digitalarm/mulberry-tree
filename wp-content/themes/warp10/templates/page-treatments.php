<?php
/**
 * Template Name: Treatments
 *
 * Description: Treatments page template.
 *
 */

get_header();
?>

<?php if ( have_posts() ) : ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'templates/template-parts/content-blocks' ); ?>

		<div class="spacing-inside">
			<div class="container">

				<?php
				// Loop Service types
				$types = get_terms( array(
					'taxonomy' => 'types',
					'order' => 'DESC',
				) );

				if ( $types ) :
				?>

					<h2 class="terms-title">Treatments</h2>

					<div class="terms">
						<?php foreach ( $types as $type ) : ?>

							<div class="term-item">
								<a href="<?php echo get_term_link( $type ); ?>" class="term-item__inner">
									<?php echo $type->name; ?>
								</a>
							</div>

						<?php endforeach; ?>
					</div>

				<?php endif; ?>

				<?php
				// Loop Service types
				$categories = get_terms( array(
					'taxonomy' => 'product_category',
					'parent' => 0,
				) );

				if ( $categories ) :
				?>

				<h2 class="terms-title">Retail Products</h2>

					<div class="terms">
						<?php foreach ( $categories as $category ) : ?>

							<div class="term-item">
								<a href="<?php echo get_term_link( $category ); ?>" class="term-item__inner">
									<?php echo $category->name; ?>
								</a>
							</div>

						<?php endforeach; ?>
					</div>

				<?php endif; ?>

			</div>
		</div>

	<?php endwhile; ?>

<?php endif; ?>

<?php get_footer(); ?>
