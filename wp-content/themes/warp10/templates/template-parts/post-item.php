<li class="post-list__item">
	<div class="post-list__item-inner">
		<?php if ( has_post_thumbnail() ) : $url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' ); ?>

			<a class="post-list__image" href="<?php the_permalink(); ?>"><span class="bg-cover" style="background-image: url(<?php echo $url[ 0 ]; ?>);"></span></a>

		<?php endif; ?>

		<div class="post-list__text">
			<a class="post-list__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			<span class="post-list__info"><?php printf( __( 'Posted on %s in ', 'w10' ), get_the_time( 'd M Y' ) ); the_category( ', ' ); ?></span>

			<div class="post-list__excerpt">
				<?php the_excerpt(); ?>
			</div>
		</div>
	</div>
</li>
