<?php $cats = get_the_category(); ?>
<li class="post-list__item<?php foreach( $cats as $cat ) : echo ' post-cat-' . $cat->slug; endforeach; ?>" id="post-<?php the_ID(); ?>">
	<div class="post-list__item-inner">
		<?php if ( has_post_thumbnail() ) : $url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' ); ?>

			<a class="post-list__image" href="<?php the_permalink(); ?>"><span class="bg-cover" style="background-image: url(<?php echo $url[ 0 ]; ?>);"></span></a>

		<?php endif; ?>

		<div class="post-list__text">
			<a class="post-list__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

			<div class="post-list__excerpt">
				<?php if ( get_the_excerpt() ) : ?>

					<?php the_excerpt(); ?>

				<?php elseif ( $metadesc = get_metadesc( get_the_ID() ) ) : ?>

					<?php echo $metadesc; ?>

				<?php endif; ?>
			</div>

			<?php if ( get_the_category() ) : ?>

				<span class="post-list__info"><?php _e( 'Posted in ', 'w10' ); the_category( ', ' ); ?></span>

			<?php endif; ?>
		</div>
	</div>
</li>
