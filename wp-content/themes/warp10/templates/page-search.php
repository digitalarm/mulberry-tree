<?php
/**
 * Template Name: Search
 *
 * Description: Search page template.
 *
 */

get_header();
?>

<?php if ( have_posts() ) : ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="block-search spacing-inside">
			<div class="container">
				<h1><?php the_title(); ?></h1>
				<?php get_sidebar(); ?>
				<div class="page-content">
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>

	<?php endwhile; ?>

<?php endif; ?>

<?php get_footer(); ?>
