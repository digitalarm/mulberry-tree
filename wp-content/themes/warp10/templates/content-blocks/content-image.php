<?php
$background_image = '';
$background_style = '';
$background_colour = '';
$custom_background_colour = '';

if ( have_rows( 'background' ) ) {

	while ( have_rows( 'background' ) ) {

		the_row();

		if ( get_sub_field( 'use_background_image' ) ) {

			if ( have_rows( 'background_image' ) ) {

				while ( have_rows( 'background_image' ) ) {

					the_row();
					$background_image = get_sub_field( 'image' );
					$background_style = get_sub_field( 'style' );

				}

			}

		}

		$background_colour = get_sub_field( 'background_colour' );

		if ( $background_colour == 'custom' ) {

			$custom_background_colour = get_sub_field( 'custom_background_colour' );

		}

	}

}

$image = get_sub_field( 'image' );
?>

<?php if ( get_sub_field( 'image_size' ) == 'cover' ) : ?>

	<?php if ( get_sub_field( 'layout' ) == 'image_left_content_right' ) {

		$content_class = '--content-right';

	} else {

		$content_class = '--content-left';

	} ?>

	<div class="block-content-image block-content-image<?php echo $content_class; ?><?php if ( $background_style ) { echo ' bg-' . $background_style; }; if ( $background_colour != 'custom' && $background_colour != 'none' ) { echo ' bg-' . $background_colour; } if ( get_sub_field( 'spacing' ) == 'outside' || get_sub_field( 'spacing' ) == 'both' ) { ?> spacing-outside<?php } ?>" style="<?php if ( $background_image ) { echo 'background-image: url(' . $background_image['url'] . ');'; } if ( $custom_background_colour ) { echo ' background-color: ' . $custom_background_colour . ';'; } ?>">
		<div class="container">
			<div class="block-content-image__content<?php if ( get_sub_field( 'spacing' ) == 'inside' || get_sub_field( 'spacing' ) == 'both' ) : ?> spacing-inside<?php endif; ?>">
				<?php the_sub_field( 'content' ); ?>
			</div>
		</div>

		<span class="block-content-image__image" style="background-image: url(<?php echo $image['url']; ?>);"></span>
	</div>

<?php else : ?>

	<div class="block-content-image<?php if ( $background_style ) { echo ' bg-' . $background_style; }; if ( $background_colour != 'custom' && $background_colour != 'none' ) { echo ' bg-' . $background_colour; } ?> spacing-<?php the_sub_field( 'spacing' ); ?> wow fadeInUp" data-wow-delay="1s" style="<?php if ( $background_image ) { echo 'background-image: url(' . $background_image['url'] . ');'; } if ( $custom_background_colour ) { echo ' background-color: ' . $custom_background_colour . ';'; } ?>">
		<div class="container">
			<div class="grid">
				<div class="block-content-image__column">
					<?php if ( get_sub_field( 'layout' ) == 'image_left_content_right' ) : ?>

						<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>">

					<?php else : ?>

						<?php the_sub_field( 'content' ); ?>

					<?php endif; ?>
				</div>

				<div class="block-content-image__column">
					<?php if ( get_sub_field( 'layout' ) == 'image_left_content_right' ) : ?>

						<?php the_sub_field( 'content' ); ?>

					<?php else : ?>

						<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>">

					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

<?php endif; ?>
