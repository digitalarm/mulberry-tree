<?php
$background_image = '';
$background_style = '';
$background_colour = '';
$custom_background_colour = '';

if ( have_rows( 'background' ) ) {

	while ( have_rows( 'background' ) ) {

		the_row();

		if ( get_sub_field( 'use_background_image' ) ) {

			if ( have_rows( 'background_image' ) ) {

				while ( have_rows( 'background_image' ) ) {

					the_row();
					$background_image = get_sub_field( 'image' );
					$background_style = get_sub_field( 'style' );

				}

			}

		}

		$background_colour = get_sub_field( 'background_colour' );
		if ( $background_colour == 'custom' ) {

			$custom_background_colour = get_sub_field( 'custom_background_colour' );

		}

	}

}
?>

<div class="block-latest-posts<?php if ( $background_style ) { echo ' bg-' . $background_style; }; if ( $background_colour != 'custom' && $background_colour != 'none' ) { echo ' bg-' . $background_colour; } ?> spacing-<?php the_sub_field( 'spacing' ); ?>" style="<?php if ( $background_image ) { echo 'background-image: url(' . $background_image['url'] . ');'; } if ( $custom_background_colour ) { echo ' background-color: ' . $custom_background_colour . ';'; } ?>">
	<div class="container">
		<ul class="post-list">
			<?php $limit = get_sub_field( 'display' );
			$width = floor( 100 / $limit );

			$args = array(
				'post_type' => 'post',
				'posts_per_page' => $limit,
				'orderby' => 'date',
				'order' => 'DESC'
			);

			$posts = get_posts( $args );

			foreach( $posts as $post ) : ?>

				<li class="post-list__item" data-width="<?php echo $width; ?>">
					<div class="post-list__item-inner">
						<?php if ( has_post_thumbnail() ) : $url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' ); ?>

							<a class="post-list__image" href="<?php the_permalink(); ?>"><span class="bg-cover" style="background-image: url(<?php echo $url[ 0 ]; ?>);"></span></a>

						<?php endif; ?>

						<div class="post-list__text">
							<a class="post-list__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<span class="post-list__info"><?php printf( __( 'Posted on %s in ', 'w10' ), get_the_time( 'd M Y' ) ); the_category( ', ' ); ?></span>

							<div class="post-list__excerpt">
								<?php the_excerpt(); ?>
							</div>

							<a class="button" href="<?php echo get_permalink( $post->ID ); ?>"><?php _e( 'Read post', 'w10' ); ?></a>
						</div>
					</div>
				</li>

			<?php endforeach; ?>
		</ul>
	</div>
</div>
