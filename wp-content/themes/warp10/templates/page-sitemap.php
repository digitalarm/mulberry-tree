<?php
/**
 * Template Name: Sitemap
 *
 * Description: Sitemap page template.
 *
 */

get_header();
?>

<?php if ( have_posts() ) : ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="spacing-inside">
			<div class="container">
				<h1><?php the_title(); ?></h1>

				<?php if ( TRUE === get_theme_mod( 'sitemap_authors', FALSE ) ) : ?>
					<h2><?php _e( 'Authors', 'w10' ); ?></h2>
					<ul>
						<?php wp_list_authors(
							array(
								'exclude_admin' => FALSE,
							)
						); ?>
					</ul>
				<?php endif; ?>

				<?php if ( TRUE === get_theme_mod( 'sitemap_pages', TRUE ) ) : ?>
					<h2><?php _e( 'Pages', 'w10' ); ?></h2>
					<ul>
						<?php // Add pages you'd like to exclude in the exclude here
						wp_list_pages(
							array(
								'exclude' => '',
								'title_li' => ''
							)
						); ?>
					</ul>
				<?php endif; ?>

				<?php /* if ( TRUE === get_theme_mod( 'sitemap_posts', TRUE ) ) : ?>
					<h2><?php _e( 'Posts', 'w10' ); ?></h2>
					<ul>
						<?php // Add categories you'd like to exclude in the exclude here
						$cats = get_categories( 'exclude=' );
						foreach ( $cats as $cat ) :
							echo "<li><h3>" . $cat->cat_name . "</h3>";
								echo "<ul>";
									query_posts( 'posts_per_page=-1&cat='.$cat->cat_ID );
									while( have_posts() ) :
										the_post();
										$category = get_the_category();
										// Only display a post link once, even if it's in multiple categories
										if ( $category[0]->cat_ID == $cat->cat_ID ) :
											echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
										endif;
									endwhile;
								echo "</ul>";
							echo "</li>";
						endforeach;
						?>
					</ul>
				<?php endif; */ ?>

				<?php /* if ( TRUE === get_theme_mod( 'sitemap_custom_posts', TRUE ) ) :
					foreach( get_post_types( array('public' => TRUE) ) as $post_type ) :
						if ( in_array( $post_type, array( 'post', 'page', 'attachment' ) ) )
							continue;

						$pt = get_post_type_object( $post_type );
						echo '<h2>' . $pt->labels->name . '</h2>';
						echo '<ul>';
							query_posts( 'post_type=' . $post_type . '&posts_per_page=-1' );
							while( have_posts() ) :
								the_post();
								echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
							endwhile;
						echo '</ul>';
					endforeach;
				endif; */ ?>
			</div>
		</div>

	<?php endwhile; ?>

<?php endif; ?>

<?php get_footer(); ?>
