<?php
get_header();
?>

<div class="block-404 spacing-inside">
	<div class="container">
		<h1><span class="fa fa-exclamation-triangle"></span><?php _e( '404 Page not found', 'w10' ); ?></h1>
		<p><?php _e( 'Sorry, that page could not be found. Please use the menu to find what you\'re looking for.', 'w10' ); ?></p>
	</div>
</div>

<?php
get_footer();
?>
