<?php
define( 'SITE_VERSION', '0.95.0' );
define( 'LIB_PATH', get_bloginfo('template_directory') . '/lib' );


/* ADD TO WP-CONFIG - Sets dev environment so that dev assets are used */
/*define( 'DEV_ENVIRONMENT', TRUE );*/

if ( TRUE === DEV_ENVIRONMENT ) {

	define( 'ASSET_DIR', 'dev' );

} else {

	define( 'ASSET_DIR', 'dist' );

}

/*
* Customizer
*
* Wordpress Customizer configuration
*/
require_once( dirname( __FILE__ ) . '/incs/customizer.php' );


/*
* ACF
*
* Custom fields of the advanced kind.
*/
require_once( dirname( __FILE__ ) . '/incs/acf-defaults.php' );


/*
* Navigation
*
* Register menus for WP admin.
*/
require_once( dirname( __FILE__ ) . '/incs/nav.php' );


/*
* Posts
*
* Register post types and post settings
*/
require_once( dirname( __FILE__ ) . '/incs/posts.php' );


/*
* Theme
*
* Load styles and scripts and any custom bits for this theme
*/
require_once( dirname( __FILE__ ) . '/incs/theme.php' );
